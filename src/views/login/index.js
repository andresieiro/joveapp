import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Title, Content , Button , Spinner} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Image } from 'react-native';
import theme from './../../theme/default';
import colors from './../../theme/colors/colors';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as userActions from './../../store/user/actions';
import * as userSelectors from './../../store/user/reducer';
import { Kaede } from 'react-native-textinput-effects';
import Logo from './../../components/logo';
import Icon from 'react-native-vector-icons/FontAwesome';

class LoginView extends Component {

  constructor(props){
    super(props);

    this.state = {
      loading:false,
    }
  }

  componentWillReceiveProps(newProps){
    this.checkUserLogin(newProps.user);
  }

  checkUserLogin(user){
    if(user.id !== undefined) {
      Actions.home();
    }
  }

  getLogin() {
    this.setState({loading:true},
      () => { this.props.dispatch(userActions.getLogin()) }
    )
  }

  render() {
    return (
      <LinearGradient colors={theme.gradients.gray} style={{...theme.containers.verticalCenter,...theme.containers.default}}>
          <View style={theme.containers.verticalCenter}>
            <Logo style={{alignSelf:'center', width:200, height:130, marginTop:0, marginBottom:50}}/>
          {
            this.state.loading ?
              <Spinner color={colors.others.placeholderGrayColor}/>
            :
              <View>
                <Kaede label="Usuario" style={{marginBottom:10}}/>
                <Kaede label="Senha"  secureTextEntry/>
                <Button block onPress={this.getLogin.bind(this)} style={{width:60,alignSelf:'center',marginTop:30,...theme.buttons.primary}}>
                  <Icon name="arrow-right" style={{color: colors.base.white}}/>
                </Button>
              </View>
            }
          </View>
      </LinearGradient>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: userSelectors.getCurrentUser(state),
  };
}

export default connect(mapStateToProps)(LoginView);
