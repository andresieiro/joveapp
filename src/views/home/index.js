import React, { Component } from 'react';
import {Container, Content, Text, Button} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Image, View } from 'react-native';
import theme from './../../theme/default';
import colors from './../../theme/colors/colors';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as userSelectors from './../../store/user/reducer';
import Icon from 'react-native-vector-icons/FontAwesome';
import SquareList from './../../components/list/squareList';
import Logo from './../../components/logo';

const style = {
  title: {
    fontSize:14,
    color:colors.base.white,
    marginBottom:20,
    marginTop:-15,
  }
}

class HomeView extends Component {

  constructor(props) {
    super(props);
  }

  goToLogin(){
    Actions.login();
  }

  onPress() {
    Actions.city();
  }
  
  render() {
    return (
      <LinearGradient colors={theme.gradients.gray} style={theme.containers.default}>
        <Container>
          <Content>

            <View style={theme.containers.horizontalCenter}>
              <Logo style={{marginBottom:30 , marginTop:30}}/>
              <Text style={style.title}>Selecione a mídia desejada:</Text>
            </View>

            <SquareList onPress={this.onPress}/>

            <View style={{flexDirection:'row', justifyContent: 'center'}}>
              <Button light block onPress={this.goToLogin} style={{marginTop:20,width:60,...theme.buttons.primary}}>
                <Icon name="arrow-left" style={{fontSize:15, color: colors.base.white}}/>
              </Button>
            </View>

          </Content>
        </Container>

      </LinearGradient>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: userSelectors.getCurrentUser(state),
  };
}

export default connect(mapStateToProps)(HomeView);
