import React, { Component } from 'react';
import {Container, Content, Text, Button, List, ListItem} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Image, View } from 'react-native';
import theme from './../../theme/default';
import colors from './../../theme/colors/colors';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Hideo } from 'react-native-textinput-effects';
import Logo from './../../components/logo';

const style = {
  title: {
    fontSize:16,
    color:colors.base.white,
    backgroundColor: 'transparent',
    alignSelf:'center',
  },
  search: {
    marginTop:30,
  }
}

class CityView extends Component {

  constructor(props) {
    super(props);
  }

  goToHome() {
    Actions.home();
  }

  render() {
    return (
      <LinearGradient colors={theme.gradients.gray} style={{...theme.containers.default}}>
        <View style={{ marginTop:30, padding:0}}>
            <Logo style={{marginBottom:30 , marginTop:30, alignSelf:'center'}}/>
            <Text style={style.title}>Qual cidade desejada?</Text>
            <Hideo
               iconClass={Icon}
               iconName={'keyboard-o'}
               iconColor={'white'}
               iconBackgroundColor={colors.base.red}
               style={style.search}
             />
           <View style={{flexDirection:'row', alignSelf:'center', marginTop:80}}>
             <Button light block style={{marginRight:10,width:60,...theme.buttons.primary}}
               onPress={this.goToHome}>
               <Icon name="arrow-left" style={{fontSize:15, color: colors.base.white}}/>
             </Button>
             <Button light block style={{width:150,...theme.buttons.primary}}>
               <Icon name="search" style={{fontSize:15, color: colors.base.white}}/>
             </Button>
           </View>
          </View>
      </LinearGradient>
    )
  }
}

function mapStateToProps(state) {
  return {
    //user: userSelectors.getCurrentUser(state),
  };
}

export default connect(mapStateToProps)(CityView);
