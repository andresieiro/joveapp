/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import {Scene, Router} from 'react-native-router-flux';
import LoginView from './../views/login';
import HomeView from './../views/home';
import CityView from './../views/city';

export default class RoutesApp extends Component {

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="login" component={LoginView} title="Login" hideNavBar={true}  />
          <Scene key="home" component={HomeView} title="Home" hideNavBar={true}  />
          <Scene key="city" component={CityView} title="Home" hideNavBar={true}  initial/>
        </Scene>
      </Router>
    );
  }
}

AppRegistry.registerComponent('RoutesApp', () => RoutesApp);
