import React, { Component } from 'react';
import { Image } from 'react-native';


export default class Logo extends Component {

  render() {
    return (
        <Image
        style={{width: 150, height: 100 ,...this.props.style}} 
        source={require('./../../assets/images/logo.png')}/>
    )
  }
}
