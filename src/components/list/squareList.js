import React, { Component } from 'react';
import { View } from 'react-native';
import SquareItem from './squareItem';

const style = {
  list: {
    flexDirection:'row',
    flex:0,
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
}

export default class SquareList extends Component {

  constructor(props){
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    this.props.onPress()
  }
  
  render() {
    return (
      <View style={style.list}>
        <SquareItem icon="television" label="Televisão" onPress={this.onPress}/>
        <SquareItem icon="headphones" label="Rádio" onPress={this.onPress}/>
        <SquareItem icon="book" label="Impressa" onPress={this.onPress}/>
        <SquareItem icon="facebook" label="Social" onPress={this.onPress}/>
      </View>
    )
  }
}
