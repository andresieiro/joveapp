import React, { Component } from 'react';
import {Button, Text} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import theme from './../../theme/default';
import colors from './../../theme/colors/colors';
import Icon from 'react-native-vector-icons/FontAwesome';

const style = {
  listItem: {
    width:150,
    height:150,
    borderWidth:4,
    borderColor: colors.base.gray,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems:'center',
    margin:5,
    padding:0,
    backgroundColor:'transparent'
  },
  text: {
    color: colors.base.white,
  },
  icon: {
    fontSize:40,
    flexWrap:'wrap',
    color: colors.base.white,
    marginBottom:10,
  },
  gradient: {
    start: {
        x: 0.0,
        y: 0.25
    } ,
    end: {
      x: 0.5,
      y: 1.0
    },
    locations: [0,0.5,0.6],
    colors: [colors.others.lighterGray, colors.others.lightGray, colors.others.lightGray,]
  }
}

export default class SquareItem extends Component {

  render() {
    return (
      <Button style={style.listItem} onPress={this.props.onPress}>
        <LinearGradient {...style.gradient} style={style.listItem} >
          <Icon name={this.props.icon} style={style.icon}/>
          <Text style={style.text}>{this.props.label}</Text>
        </LinearGradient>
      </Button>
    )
  }
}
