import colors from './colors/colors';

const theme = {
  containers : {
    default: {
      flex:1,
      flexWrap: 'wrap',
      flexDirection: 'column',
    },
    horizontalCenter: {
      alignItems: 'center',
    },
    verticalCenter: {
      justifyContent: 'center',
    },
    centerAll: {
      justifyContent: 'center',
      alignItems: 'center',
    }
  },
  inputs: {
    text: {
      marginBottom:20,
      borderWidth:1,
      borderColor:colors.others.lighterGray,
      color: colors.base.white,
    },
  },
  text: {
    default: {
      color: colors.others.lighterGray,
    }
  },
  buttons: {
    primary: {
      backgroundColor: colors.others.lighterGray,
      borderWidth: .2,
      borderColor: colors.base.white,
    },
    red: {
      backgroundColor: colors.base.red,
      borderWidth: .2,
      borderColor: colors.base.white,
    }
  },
  gradients: {
    gray: [colors.base.gray , colors.others.lightGray, colors.base.gray]
  }
};


export default theme;
