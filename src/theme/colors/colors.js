const colors = {
  base: {
    gray: '#232020',
    red: '#e71c3b',
    white: '#ffffff'
  },
  others: {
    lightGray: '#464646',
    lighterGray: '#5b5b5b',
    placeholderGrayColor: '#818181',
    blue: '#1494cc'
  }
}

export default colors;
