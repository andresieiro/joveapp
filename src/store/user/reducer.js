// reducers hold the store's state (the initialState object defines it)
// reducers also handle plain object actions and modify their state (immutably) accordingly
// this is the only way to change the store's state
// the other exports in this file are selectors, which is business logic that digests parts of the store's state
// for easier consumption by views

import _ from 'lodash';
import * as types from './actionTypes';

const initialState = {

};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.GET_LOGIN:
      return  {
        ...state,
        ...action.user
      }
    default:
      return state;
  }
}

// selectors

export function getCurrentUser(state) {
  return state.user
}
