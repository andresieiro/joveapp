import * as types from './actionTypes';
import * as userServices from './../../services/user';

export function getLogin(param) {
  return async(dispatch, getState) => {
    try {
      const user = await userServices.getLogin(param);
      dispatch({ type: types.GET_LOGIN, user:user });
    } catch (error) {
      console.error(error);
    }
  };
}
